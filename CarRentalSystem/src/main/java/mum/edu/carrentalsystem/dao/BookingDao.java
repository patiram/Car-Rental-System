package mum.edu.carrentalsystem.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import mum.edu.carrentalsystem.model.Booking;

public interface BookingDao extends JpaRepository<Booking, Integer>, Serializable{


}
