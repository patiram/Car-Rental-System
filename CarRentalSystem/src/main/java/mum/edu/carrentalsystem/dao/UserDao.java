package mum.edu.carrentalsystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import mum.edu.carrentalsystem.model.Users;

public interface UserDao extends JpaRepository<Users, Integer>{
	public Users findByUsername(String username);

}
